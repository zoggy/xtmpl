(*********************************************************************************)
(*                Xtmpl                                                          *)
(*                                                                               *)
(*    Copyright (C) 2012-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Lesser General Public           *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

(** Misc functions. *)

val string_of_file : string -> string
val split_string : ?keep_empty:bool -> string -> char list -> string list
val strip_string : string -> string
val file_of_string : file:string -> string -> unit

(** [map_string lexer string] creates an empty buffer with same
  initial length as the string, and a lexbuf from [string].
  It passes them to the [lexer], then returns the
  contents of the buffer. The lexer is supposed to fill the buffer.
  If the {!Sedlexing.Malformed} exception is raised, then the
  {!Failure} exception is raised with a message.*)
val map_string : (Buffer.t -> Sedlexing.lexbuf -> 'a) -> string -> string

(** Replace the following characters: [<] by [&lt;],
  [>] by [&gt;], [&] by [&amp;]. Also replace simple
  and double quotes by [&apos;] and [&quot;] if
  [quotes = true] (which is false by default). *)
val escape : ?quotes:bool -> string -> string

(** [string_of_pp pp x] uses pretty-printing function [pp] on [x]
  to print in a buffer and return a string. *)
val string_of_pp : (Format.formatter -> 'a -> unit) -> 'a -> string
