(*********************************************************************************)
(*                Xtmpl                                                          *)
(*                                                                               *)
(*    Copyright (C) 2012-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Lesser General Public           *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

open Js_of_ocaml
let log s = Firebug.console##log (Js.string s);;

let str = {xml|
<div><div escamp_="onclick" class="json-button" onclick="callParamsJSONFromForm(&apos;test&apos;)">json</div><div class="dmw-delayed" id="dynamowbloc__1"> ⌚ </div><form action="http://localhost:8080/services/test" id="test" onsubmit="return service_form_onsubmit(&quot;test&quot;);" parameters="test.formula" target_id="dynamowbloc__1"><input name="service" type="hidden" value="test"/><input name="ctx" type="hidden" value="[[[&quot;Service_path&quot;],[&quot;Sid_path&quot;,[&quot;test&quot;]]],[[&quot;KCounters&quot;],[&quot;Counters&quot;,[[[&quot;equation&quot;,{&quot;pred&quot;:&quot;dynamowbloc__1&quot;,&quot;succ&quot;:&quot;dynamowbloc__1_out&quot;}],[&quot;section&quot;,{&quot;pred&quot;:&quot;dynamowbloc__1&quot;,&quot;succ&quot;:&quot;dynamowbloc__1_out&quot;}]],&quot;dynamowbloc__1&quot;]]],[[&quot;Section_path&quot;],[&quot;Path&quot;,[]]]]"/><label for="test.formula">formula = </label><input id="test.formula" name="formula" title="formula" type="text" value="diff(x.x.x.x, x)"/><input id="" type="submit"/></form></div>
    |xml}

let xmls = Xtmpl.Xml.from_string str
let () = log
 ("Xtmpl.Xml.from_string ok => "^(Xtmpl.Xml.to_string xmls))
let xmls = Xtmpl.Rewrite.from_string str
let () = log
 ("Xtmpl.Rewrite.from_string ok => "^(Xtmpl.Rewrite.to_string xmls))

let node = List.map (Xtmpl_js.dom_of_xtmpl ~doc: Dom_html.document) xmls
