(*********************************************************************************)
(*                Xtmpl                                                          *)
(*                                                                               *)
(*    Copyright (C) 2012-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Lesser General Public           *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

module X = Xtmpl.Rewrite
module H = Xtmpl.Xhtml

let () =
  try
    let body = H.body [] in
    let html = H.html [body] in
    let doctype = X.doctype ("","html") "" in
    let prolog = X.prolog ~doctype [] in
    let doc = X.doc prolog [ html ] in
    print_endline (X.doc_to_string ~xml_atts:false doc)
  with
    Failure msg ->
      prerr_endline msg ; exit 1
  | Xtmpl.Types.Error e ->
      prerr_endline (Xtmpl.Types.string_of_error e);
      exit 1
  